import Vue from "vue";
import App from "./App.vue";
import "./quasar";
import VueResource from "vue-resource";
import VueRouter from "vue-router";
import { routes } from "./routes";
import { store } from "./store";

Vue.config.productionTip = false;

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: "history",
});

Vue.use(VueResource);

new Vue({
  router,
  render: (h) => h(App),
  store,
}).$mount("#app");
