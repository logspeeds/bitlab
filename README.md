# bitlab

## Organização de exames laboratoriais

```
O BITLAB é um WebApp que visa organizar e facilitar a visualização de exames laboratoriais para profissionais da saúde. Por meio de uma interface amigável o aplicativo visa alertar possíveis alterações ao profissional que pode adicionar marcações para lembrar-se quais dados são relevantes para seu diagnóstico.
```

## Tecnologia

```
• O WebApp foi desenvolvido com a tecnologia VueJS além de usar o Quasar Framework;
```

```
• O exame é puxado de uma base de dados e alocado em uma tabela (.json);
```

```
• Pode-se filtrar por exames (.filter, REGEX);
```

```
• Pode-se navegar entre duas páginas para agendar o próximo exame além de visualizá-los (router, transição, VueX[mutations. actions, state, getters]);
```

# bitlab

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
